#!/bin/usr/env/python

import os
import paramiko
#from scp import SCPClient


client=paramiko.SSHClient()
client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

#client.connect(hostname = '3.87.136.73', username = 'ubuntu', key_filename = '''IDAT.pem''')
client.connect('34.226.197.166', username = 'ubuntu', key_filename = '''IDAT.pem''')
print("copying")
ftp = client.open_sftp()
remotepath='/home/ubuntu/user-amanshah-iadt_web_application-21b33fde0a54.zip'
localpath='./user-amanshah-iadt_web_application-21b33fde0a54.zip'

ftp.put(localpath, remotepath)
#scp = SCPClient('user-amanshah-iadt_web_application-21b33fde0a54', recursive=True, remote_path='./')

bash_script = open("user_data.sh").read()
stdin, stdout, stderr = client.exec_command(bash_script)
print(stdout.read().decode())
err = stderr.read().decode()
if err:
	print(err)
ftp.close()
