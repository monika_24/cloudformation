# Cloud computing bill management system restapi 

The bill management system will be used to track the bills and vendor specific management system.
There is no UI forms in the project and is meant to be reference for a backend application
No UI and REST API only



**We will be using python based flask module for developing web rest api. Prior knowledge to flask development is needed**


## How does it work????

There are certain requirements to be met 

1. You need to install database server postgres for the application to work.

2. Install from requirments.txt  to add the dependency to the project by running command

3. run "pip3 install -r requirements.txt"

4. run command "python3 views.py"


### For the end points the rules are following

1. (v1/user) First sign up for bill management by creating login. Enter A-z,a-z and special symbol with digits and 8 len password
2. After creating profile , see the profile detail (v1/user/self) - copy the UUID generated.

3. (v1/bill/) Create a bill by using the owner_id you copied. and fill rest of the details.
4. If successfull you have created a valid bill


# NOt included for the assignment avoid this

### Continuous integreation using Circle CI
 we will be using conytinuous integration by circle ci platform. Make your github account linked with it.

1. You need to enable 3rd party access on your github account for circle ci to work

2. Import your github project in circle ci account to build test and workflows
3. yaml file is include , make appropriate changes accorting to your environment. We have used ubuntu docker and postgres image.

5. run command "python3 -m test test_app.py"
6. #comment

## code deploy part

triggering code deploy from change in config.yml
creatin new pull request must trigger code deploy
## create code -deploy and cloudwatch 