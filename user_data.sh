 #!/bin/bash

sudo apt-get update -y
sudo apt-get upgrade -y

sudo apt-get python3-pip -y
sudo apt-get install unzip -y
unzip user-amanshah-iadt_web_application-21b33fde0a54.zip
cd user-amanshah-iadt_web_application-21b33fde0a54
pip3 install -r requirements.txt
nohup python3 views.py &

#bash commands for stress

sudo apt-get update
#sudo mv /var/lib/dpkg/info/install-info.postinst /var/lib/dpkg/info/install-info.postinst.bad
#sudo apt-get upgrade -y
sudo apt-get install stress -y
stress --cpu 1 --timeout 400
