variable "aws_region" {
	default = "us-east-1"
}

variable "vpc_cidr" {
	default = "10.0.0.0/16"
}

variable "subnets_cidr" {
	type = list
	default = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
}
variable "azs" {
	type = list
	default = ["us-east-1a", "us-east-1b", "us-east-1c"]
}
	
variable "dbsubnets_cidr1" {
        default = "10.0.4.0/24"
}
variable "dbaz" {
        default = "us-east-1d"
}
variable "dbsubnets_cidr2" {
        default = "10.0.5.0/24"
}
variable "dbazs1" {
        default = "us-east-1e"
}

variable "instance_count" {
  	default = "1"
}

variable "instance_tags" {
  	type = list
  	default = ["Webserver"]
}
variable "instance_count1" {
        default = "1"
}

variable "instance_tags1" {
        type = list
        default = ["Database"]
}

variable "webservers_ami" {
  	default = "ami-014de7dca625f5c1b"
}

variable "instance_type" {
  	default = "t2.micro"
}


