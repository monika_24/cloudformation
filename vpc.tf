# VPC
resource "aws_vpc" "terra_vpc" {
	cidr_block = "${var.vpc_cidr}"
  	tags = {
    		Name = "TerraVPC"
  	}
}

# Internet Gateway
resource "aws_internet_gateway" "terra_igw" {
  	vpc_id = "${aws_vpc.terra_vpc.id}"
  	tags = {
    		Name = "main"
  	}
}

# Subnets : public
resource "aws_subnet" "public" {
  	count = "${length(var.subnets_cidr)}"
  	vpc_id = "${aws_vpc.terra_vpc.id}"
  	cidr_block = "${element(var.subnets_cidr,count.index)}"
  	availability_zone = "${element(var.azs,count.index)}"
  	tags = {
    		Name = "Subnet-public"
  	}
}

# private subnet1

resource "aws_subnet" "sub1" {
        vpc_id = "${aws_vpc.terra_vpc.id}"
        cidr_block = "${var.dbsubnets_cidr1}"
        availability_zone = "${var.dbaz}"
        tags = {
                Name = "db_subnet1"
        }
}

resource "aws_subnet" "sub2" {
        vpc_id = "${aws_vpc.terra_vpc.id}"
        cidr_block = "${var.dbsubnets_cidr2}"
        availability_zone = "${var.dbazs1}"
        tags = {
                Name = "db_subnet2"
        }
}

# Route table: attach Internet Gateway 
resource "aws_route_table" "public_rt" {
  	vpc_id = "${aws_vpc.terra_vpc.id}"
  	route {
    		cidr_block = "0.0.0.0/0"
    		gateway_id = "${aws_internet_gateway.terra_igw.id}"
  	}
  	tags = {
    		Name = "publicRouteTable"
  	}
}
#route table :
resource "aws_route_table" "private_rt" {
        vpc_id = "${aws_vpc.terra_vpc.id}"
        tags = {
                Name = "privateRouteTable"
        }
}

# Route table association with public subnets
resource "aws_route_table_association" "a" {
  	count = "${length(var.subnets_cidr)}"
  	subnet_id      = "${element(aws_subnet.public.*.id,count.index)}"
  	route_table_id = "${aws_route_table.public_rt.id}"
}


# creating db instance 

resource "aws_db_subnet_group" "db" {
	name = "db_group"
	description = "database subnet group"
	subnet_ids = ["${aws_subnet.sub1.id}", "${aws_subnet.sub2.id}"]
}

resource "aws_db_instance" "db" {
	allocated_storage = 8
	storage_type = "gp2"
	engine = "postgres"
	engine_version = "9.6.11"
	instance_class = "db.t2.micro"
	name = "DBInstance"
	username = "Monika"
	password = "Monika2412"
	db_subnet_group_name = "${aws_db_subnet_group.db.id}"
	vpc_security_group_ids = ["${aws_security_group.sgdb.id}"]
}

output "rds_endpoint" {
	value = "${aws_db_instance.db.endpoint}"
}


#load balancer

resource "aws_key_pair" "IDAT" {
        key_name = "IDAT"
        public_key = "${file("IDAT.pem")}"
}
data "template_file" "userdata" {
       template = "${file("/home/ubuntu/cloudformation/user.sh")}"
       vars = {
               RDShostname = "${aws_db_instance.db.endpoint}"
       }
}
resource "aws_launch_configuration" "web" {
  	name_prefix = "webserver"
  	image_id = var.webservers_ami
  	instance_type = var.instance_type
  	key_name = "${aws_key_pair.IDAT.key_name}"
  	security_groups = ["${aws_security_group.sgweb.id}"]
  	associate_public_ip_address = true
	user_data     = "${data.template_file.userdata.rendered}" 
  
  	lifecycle {
    		create_before_destroy = true
  	}
}

resource "aws_elb" "web_elb" {
	name = "web-elb"
	security_groups = [ "${aws_security_group.sgweb.id}" ]
	subnets =  "${aws_subnet.public.*.id}" 
	cross_zone_load_balancing = true
	health_check {
		healthy_threshold = 2
		unhealthy_threshold = 2
		timeout = 3
		interval = 30
		target = "HTTP:80/"
	}
	listener {
		lb_port = 80
		lb_protocol = "http"
		instance_port = "80"
		instance_protocol = "http"
	}
}

resource "aws_autoscaling_group" "web" {
	name = "asg"
	
	min_size = 1
	desired_capacity = 1
	max_size =3
	
	health_check_type = "ELB"
	load_balancers= [ "${aws_elb.web_elb.id}" ]
	
	launch_configuration = "${aws_launch_configuration.web.name}"
	enabled_metrics = [
		"GroupMinSize",
		"GroupMaxSize",
		"GroupDesiredCapacity",
		"GroupInServiceInstances",
		"GroupTotalInstances"
	]
	metrics_granularity="1Minute"
	
	vpc_zone_identifier = "${aws_subnet.public.*.id}"
	

	lifecycle {
		create_before_destroy = true
	}

	tags = [
		{
			key = "Name"
			value = "web"
			propagate_at_launch = true
		}
	]
}

output "ELB_IP" {
	value = "${aws_elb.web_elb.dns_name}"
}
